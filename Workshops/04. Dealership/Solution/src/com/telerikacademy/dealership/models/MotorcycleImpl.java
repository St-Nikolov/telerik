package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private static final String CATEGORY_FIELD = "Category";

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        Validator.ValidateNull(category,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,CATEGORY_FIELD));
        Validator.ValidateIntRange(category.length(),ModelsConstants.MIN_CATEGORY_LENGTH,ModelsConstants.MAX_CATEGORY_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,CATEGORY_FIELD,
                        ModelsConstants.MIN_CATEGORY_LENGTH,ModelsConstants.MAX_CATEGORY_LENGTH));
        this.category=category;
    }

    public String getCategory() {
        return category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %s",CATEGORY_FIELD,category);
    }
}
