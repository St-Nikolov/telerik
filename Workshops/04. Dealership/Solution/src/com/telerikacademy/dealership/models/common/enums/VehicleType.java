package com.telerikacademy.dealership.models.common.enums;

public enum VehicleType {
    
    MOTORCYCLE(2),
    CAR(4),
    TRUCK(8);
    private final int vehicleTypeCode;
    
    VehicleType(int vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }
    
    public int getWheelsFromType() {
        return vehicleTypeCode;
    }
}


