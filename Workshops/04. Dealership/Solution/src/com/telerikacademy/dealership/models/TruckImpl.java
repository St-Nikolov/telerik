package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private static final String CAPACITY_FIELD = "Weight capacity";

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        Validator.ValidateIntRange(weightCapacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,CAPACITY_FIELD,ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
        this.weightCapacity=weightCapacity;
    }

    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %dt",CAPACITY_FIELD,weightCapacity);
    }
}
