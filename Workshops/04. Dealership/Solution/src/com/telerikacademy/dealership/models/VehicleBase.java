package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private final static String TYPE_FIELD = "Type";
    
    private VehicleType type;
    private String make;
    private String model;
    private double price;
    private List<Comment> comments;
    
    protected VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        this.make=make;
        this.model=model;
        this.price=price;
        type = vehicleType;
        comments = new ArrayList<>();

        validateState();
    }

    public VehicleType getType() {
        return type;
    }

    public int getWheels() {
        return type.getWheelsFromType();
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }


    // I don't validate the data in next two methods, because it's already validated!
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        builder.append(String.format("  %s: %s",MAKE_FIELD,make)).append(System.lineSeparator());
        builder.append(String.format("  %s: %s",MODEL_FIELD,model)).append(System.lineSeparator());
        builder.append(String.format("  %s: %d",WHEELS_FIELD,getWheels())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        String additionalInfo = printAdditionalInfo();
        if (!additionalInfo.isEmpty()) {
            builder.append(additionalInfo).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //This method will be overridden in the subclasses and will return specific result depending on the class its called from.
    protected abstract String printAdditionalInfo();
    
    // I haven't validated number of the wheels, because they are part of VehicleType Enum!

    private void validateState(){

        Validator.ValidateNull(make,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MAKE_FIELD));
        Validator.ValidateNull(model,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MODEL_FIELD));
        Validator.ValidateNull(type,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,TYPE_FIELD));


        Validator.ValidateIntRange(make.length(),ModelsConstants.MIN_MAKE_LENGTH,ModelsConstants.MAX_MAKE_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,MAKE_FIELD,ModelsConstants.MIN_MAKE_LENGTH,ModelsConstants.MAX_MAKE_LENGTH));

        Validator.ValidateIntRange(model.length(),ModelsConstants.MIN_MODEL_LENGTH,ModelsConstants.MAX_MODEL_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,MODEL_FIELD,ModelsConstants.MIN_MODEL_LENGTH,ModelsConstants.MAX_MODEL_LENGTH));

        Validator.ValidateDecimalRange(price,ModelsConstants.MIN_PRICE,ModelsConstants.MAX_PRICE,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,PRICE_FIELD,ModelsConstants.MIN_PRICE,ModelsConstants.MAX_PRICE));

    }

    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.isEmpty()) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
    
}
