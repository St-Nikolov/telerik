package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Locale;

public class ProductBase implements Product {
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final String PRODUCT_NAME_LENGTH_ERR = "Product name must be between %d and %d characters";
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final String BRAND_NAME_LENGTH_ERR = "Brand name must be between %d and %d characters";
    public static final String NEGATIVE_PRICE_ERR = "Price cannot be negative";
    //Finish the class
    //implement proper interface (see contracts package)
    //validate

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    
    ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    public String print(){
        return String.format("#%s %s%n" +
                " #Price: %.2f%n" +
                " #Gender: %s%n",
                name,brand,price,gender);
    }

    private void setName(String name) {
        if(isTextLengthInRange(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH)) this.name = name;
        else throw new IllegalArgumentException(String.format(PRODUCT_NAME_LENGTH_ERR,NAME_MIN_LENGTH,NAME_MAX_LENGTH));
    }

    private void setBrand(String brand) {
        if(isTextLengthInRange(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH)) this.brand = brand;
        else throw new IllegalArgumentException(String.format(BRAND_NAME_LENGTH_ERR,BRAND_MIN_LENGTH,BRAND_MAX_LENGTH));
    }

    private void setPrice(double price) {
        if (price >= 0) this.price = price;
        else throw new IllegalArgumentException(NEGATIVE_PRICE_ERR);
    }

    private boolean isTextLengthInRange(String text,int minLength,int maxLength){
        return (text!=null && text.length()>=minLength && text.length()<=maxLength);
    }

}
