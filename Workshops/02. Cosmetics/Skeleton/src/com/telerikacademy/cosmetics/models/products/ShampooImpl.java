package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl extends ProductBase implements Shampoo {

    public static final String NEGATIVE_VOLUME_ERR = "Volume cannot be negative";
    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        this.usage=usage;
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return usage;
    }

    private void setMilliliters(int milliliters){
        if(milliliters>=0) this.milliliters = milliliters;
        else throw new IllegalArgumentException(NEGATIVE_VOLUME_ERR);
    }

    @Override
    public String print() {
        return super.print() + String.format(" #Milliliters: %d" +
                "%n #Usage: %s%n",milliliters,usage) ;
    }
}
