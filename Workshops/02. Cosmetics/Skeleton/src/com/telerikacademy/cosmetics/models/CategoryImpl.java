package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    public static final int CATEGORY_NAME_MIN_LENGTH = 5;
    public static final int CATEGORY_NAME_MAX_LENGTH = 30;
    public static final String CATEGORY_NAME_LENGTH_ERR = "Category name must be between %d and %d characters";
    public static final String PRODUCT_CANNOT_BE_NULL = "Product cannot be null";
    public static final String DELETE_PRODUCT_ERR = "%s not found in %s category";

    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        if(name!=null && name.length() >= 5 && name.length() <= 30)  this.name = name;
        else throw new IllegalArgumentException(String.format(CATEGORY_NAME_LENGTH_ERR, CATEGORY_NAME_MIN_LENGTH, CATEGORY_NAME_MAX_LENGTH));
        products = new ArrayList<>();
    }
    
    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //We return copy of the list, so we don't give access to the original list, however one have access to the elements of the list, so there should be another way to act.
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product==null) throw new IllegalArgumentException(PRODUCT_CANNOT_BE_NULL);
        if (products.contains(product)) return; // It's good to do something
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if(! products.remove(product)) throw new IllegalArgumentException(String.format(DELETE_PRODUCT_ERR,product.getName(),getName()));
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        StringBuilder print = new StringBuilder("#Category: " + name + "\n");
        if (products.size() == 0) {
            return print + " #No product in this category";
        }
        for (Product product: products) {
            print.append(product.print());
            print.append(" ===");
        }
        return print.toString();
    }
    
}
