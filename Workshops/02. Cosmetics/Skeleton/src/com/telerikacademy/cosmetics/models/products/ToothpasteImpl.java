package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private static final String INGREDIENTS_NULL_ERR = "Ingredients cannot be null";

    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        if(ingredients==null) throw new IllegalArgumentException(INGREDIENTS_NULL_ERR);
        this.ingredients = ingredients;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        StringBuilder print = new StringBuilder(super.print());
        print.append(" #Ingredients: [");
        for (String ingredient: ingredients) {
            print.append(ingredient + ", ");
        }
        print.append("\b\b]\n");
        return print.toString();
    }
}
