package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private static final int PRODUCT_NAME_MIN_LENGTH = 3;
    private static final int PRODUCT_NAME_MAX_LENGTH = 15;
    private static final int BRAND_NAME_MIN_LENGTH = 2;
    private static final int BRAND_NAME_MAX_LENGTH = 10;

    private double price;
    private String name;
    private String brand;
    private GenderType gender;
    
    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }
    
    public String print() {
        return String.format(" #%s %s%n" +
                 " #Price: $%.2f%n" +
                 " #Gender: %s%n" +
                 " ===%n",name,brand,price,gender);

        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }

    private void setPrice(double price) {
        if(price<0) throw new IllegalArgumentException("Price cannot be negative number");
        this.price = price;
    }

    private void setName(String name) {
        if(name.length()< PRODUCT_NAME_MIN_LENGTH || name.length()> PRODUCT_NAME_MAX_LENGTH)
            throw new IllegalArgumentException(String.format("Product's name length must be at least %d and maximum %d symbols",PRODUCT_NAME_MIN_LENGTH,PRODUCT_NAME_MAX_LENGTH));
        this.name = name;
    }

    private void setBrand(String brand) {
        if(brand.length()< BRAND_NAME_MIN_LENGTH || brand.length()> BRAND_NAME_MAX_LENGTH)
            throw new IllegalArgumentException(String.format("Brand's name length must be at least %d and maximum %d symbols",BRAND_NAME_MIN_LENGTH,BRAND_NAME_MAX_LENGTH));
        this.brand = brand;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public double getPrice() {
        return price;
    }
}
