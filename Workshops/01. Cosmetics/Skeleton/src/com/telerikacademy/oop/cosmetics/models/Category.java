package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String NAME_INVALID_MESSAGE =
            String.format("Category's name length must be at least %d and maximum %d symbols",
                    NAME_MIN_LENGTH,NAME_MAX_LENGTH);
    private String name;
    private List<Product> products;

    private void setName(String name) {
        if(name.length()< NAME_MIN_LENGTH || name.length()> NAME_MAX_LENGTH) throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        this.name = name;
    }

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }
    
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if(product==null) throw new IllegalArgumentException("addProduct:Product cannot be null");
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if(product==null) throw new IllegalArgumentException("removeProduct:Product cannot be null");
        if(!products.remove(product)) throw new IllegalArgumentException("removeProduct:Product not found in category: " + name);
    }
    
    public String print() {
        StringBuilder builder = new StringBuilder("#Category: " + name + "\n");
        if(products.size()==0) builder.append(" #No products in this category");
        for (Product product: products) {
            builder.append(product.print());
        }
        return builder.toString();
    }
    
}
