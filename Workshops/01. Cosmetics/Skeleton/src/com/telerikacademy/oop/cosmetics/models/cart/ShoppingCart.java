package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        productList = new ArrayList<Product>();
    }
    
    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }
    
    public void addProduct(Product product) {
        if(product==null) throw new IllegalArgumentException("addProduct:Product cannot be null");
        productList.add(product);
    }
    
    public void removeProduct(Product product) {
        if(product==null) throw new IllegalArgumentException("removeProduct:Product cannot be null");
        productList.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        if(product==null) throw new IllegalArgumentException("containsProduct:Product cannot be null");
        return  productList.contains(product);
    }
    
    public double totalPrice() {
        double totalPrice=0;
        for (Product product: productList) {
            totalPrice+=product.getPrice();
        }
        return totalPrice;
    }
    
}
