package com.telerikacademy.lecture01;

import java.util.Scanner;

public class TreeGroups {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] numbersAsString = scanner.nextLine().split("\\s");
        scanner.close();
        String[] result = {"", "", ""};
        int number;
        for (String numberAsString : numbersAsString) {
            int remainder = Integer.parseInt(numberAsString) % 3;
            result[remainder] = result[remainder].concat(numberAsString + " ");
        }
        for (String line : result) {
            System.out.println(line);
        }
    }
}
