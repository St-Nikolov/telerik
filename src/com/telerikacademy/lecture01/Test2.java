package com.telerikacademy.lecture01;


import java.util.Arrays;
import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] line = sc.nextLine().split(" ");
        int[] input = new int[line.length];
        for (int i = 0; i < line.length; i++) {
            input[i] = Integer.parseInt(line[i]);
        }
        int[] firstArray = new int[input[0]];
        int[] secondArray = new int[input[1]];
        for (int i = 0; i < (input[0]); i++) {
            firstArray[i] = sc.nextInt();
        }
        for (int i = 0; i < (input[1]); i++) {
            secondArray[i] = sc.nextInt();
        }
        String arr1 = Arrays.toString(firstArray).replaceAll("\\[|\\]|,|\\s", "");
        String arr2 = Arrays.toString(secondArray).replaceAll("\\[|\\]|,|\\s", "");
        String arrRev1;
        String arrRev2;
        String reverse1 = new StringBuilder(arr1).reverse().toString();
        String reverse2 = new StringBuilder(arr2).reverse().toString();

        int num1 = Integer.parseInt(reverse1);
        int num2 = Integer.parseInt(reverse2);
        int sum = num1 + num2;
        int reversedSum = 0;
        while (sum != 0) {
            int digit = sum % 10;
            reversedSum = reversedSum * 10 + digit;
            sum /= 10;
        }
        System.out.println(reversedSum);
    }
}