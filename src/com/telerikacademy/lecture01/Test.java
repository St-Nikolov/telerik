package com.telerikacademy.lecture01;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size = Integer.parseInt(sc.nextLine());
        int[] list = new int[size];
        int sequence = 1;
        int max = 1;
        for (int i = 0; i < size; i++) {
            list[i] = Integer.parseInt(sc.nextLine());
        }
        for (int j = 1; j < list.length; j++) {
            if (list[j] == list[j - 1]) {
                sequence++;
            } else {
                sequence = 1;
            }
            if (sequence > max) {
                max = sequence;
            }
        }
        System.out.println(max);
    }
}