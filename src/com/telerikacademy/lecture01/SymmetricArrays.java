package com.telerikacademy.lecture01;

import java.util.Scanner;

public class SymmetricArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int countLines = Integer.parseInt(scanner.nextLine());
        boolean [] symmetric = new boolean[countLines];
        for (int i = 0; i < countLines; i++) {
            String line = scanner.nextLine();
            String [] array = line.split("\\s");
            symmetric[i] = isSymmetric(array);
        }
        scanner.close();
        for (boolean isSymmetric :symmetric) {
            System.out.println(isSymmetric ? "Yes" : "No");
        }
    }

    public static boolean isSymmetric(String[] array){
        for (int i = 0; i < array.length/2; i++) {
            if (!array[i].equals(array[array.length-1-i])) return false;
        }
        return true;
    }
}
