package com.telerikacademy.lecture01;

import java.util.Scanner;

public class SpiralMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        int offset = 0;

        int[][] matrix = new int[num][num];
        int counter = 1;
        int border = num-1;
        while (counter <= num*num) {
            for (int i = offset; i <= border; i++) {
                matrix[offset][i] = counter;
                counter++;
            }
            for (int i = offset+1; i <= border; i++) {
                matrix[i][border] = counter;
                counter++;
            }
            for (int i = border-1; i >= offset; i--) {
                matrix[border][i] = counter;
                counter++;
            }
            for (int i = border-1; i >= offset+1; i--) {
                matrix[i][offset] = counter;
                counter++;
            }
            offset++;
            border--;
        }

        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix){
        for (int[] row:matrix) {
            for (int cell:row) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }
}
