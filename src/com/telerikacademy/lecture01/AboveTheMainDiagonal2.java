package com.telerikacademy.lecture01;

import java.util.Scanner;

public class AboveTheMainDiagonal2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixSize = scanner.nextInt();
        scanner.close();
//        int sum = 0;
//        int biggestExponent = 2 * (matrixSize - 1);
//        for (int i = 0; i < matrixSize - 1; i++) {
//            sum += Math.pow(2, i) * (i / 2 + 1);
//            sum += Math.pow(2, biggestExponent - i) * (i / 2 + 1);
//        }
//        sum += Math.pow(2, matrixSize - 1) * ((matrixSize - 1) / 2 + 1);
//        System.out.println(sum);

        long matrix[][] = fillMatrix(matrixSize);
        System.out.println(sumAboveMainDiagonal(matrix));
    }

    public static long sumAboveMainDiagonal(long[][] matrix){
        long sum = 0;
        for (int col = 0; col < matrix.length; col++) {
            for (int row = col; row < matrix[col].length; row++) {
                sum+=matrix[row][col];
            }
        }
        return sum;
    }

    public static long[][] fillMatrix(int number){
        long[][] matrix = new long[number][number];
        for (int row = 0; row < number; row++){
            for (int col = 0; col < number; col++) {
                matrix[row][col] = powerOfTwo(row + col);
            }
        }
        return matrix;
    }

    private static long powerOfTwo(int exponent){
        long pow=1;
        for (int i = 0; i < exponent; i++) {
            pow*=2;
        }
        return pow;
    }
}
