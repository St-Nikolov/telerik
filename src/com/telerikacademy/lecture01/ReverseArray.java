package com.telerikacademy.lecture01;

import java.util.Scanner;

public class ReverseArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String numbers = scanner.nextLine();
        scanner.close();
        String [] numbersArray = numbers.split("\\s");
        for (int i = numbersArray.length-1; i > 0; i--) {
            System.out.print(numbersArray[i] + ",");
        }
        System.out.println(numbersArray[0]);

    }
}
