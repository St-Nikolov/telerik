package com.telerikacademy.lecture01;

import java.util.Scanner;

public class BigNumbers {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int sizeOfArray1 = scanner.nextInt();
        int sizeOfArray2 = scanner.nextInt();
        int[] array1 = readArray(sizeOfArray1);
        int[] array2 = readArray(sizeOfArray2);
        scanner.close();

        sumAndPrint(array1,array2);
    }

    private static int[] readArray(int size){
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    private static void sumAndPrint(int[] array1,int[] array2){
        if (array1.length < array2.length) sumAndPrint(array2,array1);
        else {
            int keepInMind = 0;
            for (int i = 0; i < array2.length; i++) {
                int sumOfDigits = array1[i] + array2[i] + keepInMind;
                keepInMind = sumOfDigits/10;
                System.out.printf("%d ",sumOfDigits%10);
            }
            for (int i = array2.length; i < array1.length ; i++) {
                int sumOfDigits = array1[i] + keepInMind;
                keepInMind = sumOfDigits/10;
                System.out.printf("%d ",sumOfDigits%10);
            }
        }
    }
}
