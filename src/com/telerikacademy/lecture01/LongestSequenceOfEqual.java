package com.telerikacademy.lecture01;

import java.util.Scanner;

public class LongestSequenceOfEqual {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numbersCount = Integer.parseInt(scanner.nextLine());
        int current;
        int previous = Integer.parseInt(scanner.nextLine());
        int longestSequence = 1;
        int currentSequence = 1;

        for (int i = 1; i < numbersCount; i++) {
            current = Integer.parseInt(scanner.nextLine());
            if (current == previous) currentSequence++;
            else {
                previous = current;
                if (currentSequence > longestSequence) {
                    longestSequence = currentSequence;
                }
                currentSequence = 1;
            }
        }
        System.out.println(longestSequence>currentSequence ? longestSequence : currentSequence);
    }
}
