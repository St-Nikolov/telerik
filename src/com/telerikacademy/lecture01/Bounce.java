package com.telerikacademy.lecture01;

import java.util.Scanner;

public class Bounce {
    private static Scanner scanner = new Scanner(System.in);
    private static long[][] matrix;
    private static int matrixHeight;
    private static int matrixLength;

    private static enum MoveDirection {NE,SE,SW,NW};
    private static enum BorderDirections {NORTH,SOUTH,EAST,WEST}

    public static void main(String[] args) {
        matrixHeight = scanner.nextInt();
        matrixLength = scanner.nextInt();

        if (matrixHeight == 1 || matrixLength ==1) {
            System.out.println(1);
            return;
        }

        matrix = initMatrix(matrixHeight, matrixLength);
        System.out.println(move(1,1, MoveDirection.SE));
    }

    private static long move(int row, int col, MoveDirection direction) {
        long sum = 5;
        BorderDirections wall;
        while (!cornerIsReached(row, col)) {
            wall = wallIsReached(row, col);
            if (wall != null) direction = changeDirection(direction, wall);
            switch (direction) {
                case NE:
                    row--;
                    col++;
                    break;
                case SE :
                    row++;
                    col++;
                    break;
                case SW :
                    row++;
                    col--;
                    break;
                case NW :
                    row--;
                    col--;
                    break;
            }
            sum+=matrix[row][col];
        }
        return sum;
    }

    private static MoveDirection changeDirection(MoveDirection direction, BorderDirections wall) {
        switch (wall) {
            case NORTH: {
                switch (direction) {
                    case NW:
                        return MoveDirection.SW;
                    case NE:
                        return MoveDirection.SE;
                }
            }
            case EAST: {
                switch (direction) {
                    case NE:
                        return  MoveDirection.NW;
                    case SE:
                        return MoveDirection.SW;
                }
            }
            case SOUTH: {
                switch (direction) {
                    case SW:
                        return  MoveDirection.NW;
                    case SE:
                        return  MoveDirection.NE;
                }
            }
            case WEST: {
                switch (direction) {
                    case NW:
                        return MoveDirection.NE;
                    case SW:
                        return MoveDirection.SE;
                }
            }
        }
        return null;
    }

        private static BorderDirections wallIsReached ( int row, int col){
            if (row == 0) return BorderDirections.NORTH;
            if (row == matrixHeight - 1) return BorderDirections.SOUTH;
            if (col == 0) return BorderDirections.WEST;
            if (col == matrixLength - 1) return BorderDirections.EAST;
            return null;
        }

        private static boolean cornerIsReached ( int row, int col){
            return (row == matrixHeight - 1 && col == matrixLength - 1)
                    || (row == 0 && col == matrixLength - 1)
                    || (row == matrixHeight - 1 && col == 0)
                    || (row == 0 && col == 0);
        }

        private static long[][] initMatrix ( int matrixHeight, int matrixLength){
            long[][] matrix = new long[matrixHeight][matrixLength];
            for (int i = 0; i < matrixHeight; i++) {
                for (int j = 0; j < matrixLength; j++) {
                    matrix[i][j] = powerOfTwo(i + j);
                }
            }
            return matrix;
        }

        private static long powerOfTwo ( int exponent){
            long pow = 1;
            for (int i = 0; i < exponent; i++) {
                pow *= 2;
            }
            return pow;
        }

    }
