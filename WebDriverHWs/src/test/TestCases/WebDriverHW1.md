# Stanislav Nikolov
#WebDriver HW Test cases for checking topic creation with too short title and content.

## TC 1

### Title: Should be impossible to create a new topic with a very short title.
### Narrative: Trying to post new a topic with a title shorter than 5 symbols should be impossible, proper message should appear

### Prerequisites:
* **User name:** stanislav.nikolov.a28@learn.telerikacademy.com
* **Password:** Mechka
* **URL:** https://stage-forum.telerikacademy.com/
* **Expected error message:**  Title must be at least 5 characters

### Steps:
| |Step |Expected result |
|:---|:----|:----|
|1.| Navigate | Forum page should be loaded|
|2.| Click on Log in button | Sign up/Log in window should appear |
|3.| Log in with the presented credentials | Forum page, with the logged user should be loaded. |
|4.|Click on New Topic button| New topic field should be opened at the bottom of the window|
|5.|Fill in the title text field with "1234" |  "1234" should be seen in the title field|
|6.|Fill in the comment text field "1234567890" | "1234567890" should be seen in the comment text field|
|7.|Click on Create Topic button. | "Title must be at least 5 characters" message should be shown in the corresponding field. A topic should not be created.|

## TC 2

### Title: Should be impossible to create a new topic with a very short content.
### Narrative: Trying to post a new topic with content shorter than 10 symbols should be impossible, proper message should appear.

### Prerequisites:
* **User name:** stanislav.nikolov.a28@learn.telerikacademy.com
* **Password:** Mechka
* **URL:** https://stage-forum.telerikacademy.com/
* **Expected error message:**  Post must be at least 10 characters


### Steps:
| |Step |Expected result |
|:---|:----|:----|
|1.| Navigate | Forum page should be loaded|
|2.| Click on Log in button | Sign up/Log in window should appear |
|3.| Log in with the presented credentials | Forum page, with the logged user should be loaded. |
|4.|Click on New Topic button| New topic field should be opened at the bottom of the window|
|5.|Fill in the title text field with "12345" |  "12345" should be seen in the title field|
|6.|Fill in the comment text field "123456789" | "123456789" should be seen in the comment text field|
|7.|Click on Create Topic button. | "Post must be at least 10 characters" message should be shown in the corresponding field. A topic should not be created.|

## TC 3

### Title: Should be impossible to create a new topic with a very short title and content.
### Narrative: Trying to post new a topic with a title shorter than 5 symbols and content shorter than 10 symbols should be impossible, both proper messages should appear.

### Prerequisites:
* **User name:** stanislav.nikolov.a28@learn.telerikacademy.com
* **Password:** Mechka
* **URL:** https://stage-forum.telerikacademy.com/
* **Expected error message:**  Title must be at least 5 characters
* **Expected error message:**  Post must be at least 10 characters


### Steps:
| |Step |Expected result |
|:---|:----|:----|
|1.| Navigate | Forum page should be loaded|
|2.| Click on Log in button | Sign up/Log in window should appear |
|3.| Log in with the presented credentials | Forum page, with the logged user should be loaded. |
|4.|Click on New Topic button| New topic field should be opened at the bottom of the window|
|5.|Fill in the title text field with "1234". |  "1234" should be seen in the title field|
|6.|Fill in the comment text field "123456789". | "123456789" should be seen in the comment text field|
|7.|Click on Create Topic button. | "Title must be at least 5 characters" and "Post must be at least 10 characters" messages should be shown in the corresponding fields. A topic should not be created.|
