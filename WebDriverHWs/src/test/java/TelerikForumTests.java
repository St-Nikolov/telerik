import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TelerikForumTests  {
    private static final String SIGN_IN_WINDOW_TITLE = "Sign in";
    private static final String EMAIL_INPUT_ID = "Email";
    private static final String PASSWORD_INPUT_ID = "Password";
    private static final String LOGIN_BUTTON_IN_SIGNIN_SCREEN_ID = "next";

    private static final String LOGIN_BUTTON_XPATH = "//button[contains(@class,'login-button')]";
    private static final String NEW_TOPIC_ID = "create-topic";
    private static final String TITLE_FIELD_ID = "reply-title";
    private static final String CREATE_TOPIC_BUTTON_XPATH = "//span[contains(text(),'Create Topic')]";
    private static final String TEXTAREA_FIELD_CSS = "textarea";

    private static final String EXPECTED_MESSAGE_FOR_SHORT_TITLE = "'Title must be at least 5 characters'";
    private static final String TITLE_TOO_SHORT_MESSAGE_XPATH = "//div[contains(text()," + EXPECTED_MESSAGE_FOR_SHORT_TITLE + ")]";
    private static final String EXPECTED_MESSAGE_FOR_SHORT_POST = "'Post must be at least 10 characters'";
    private static final String POST_TOO_SHORT_MESSAGE_XPATH = "//div[contains(text()," + EXPECTED_MESSAGE_FOR_SHORT_POST + ")]";

    private static final String TEST_FAIL_ERROR_MESSAGE = "Expected message didn't appear";

    private static final String CANCEL_TOPIC_BUTTON_XPATH = "//a[contains(text(),'cancel')]";
    private static final String CONFIRM_CANCEL_BUTTON_XPATH = "//span[contains(text(),'Yes, abandon')]";

    private String URL = "https://stage-forum.telerikacademy.com/";
    private String USER = " stanislav.nikolov.a28@learn.telerikacademy.com";
    private String PASS = "Mechka";
    private String TEST_TITLE_SHORT = "1234";
    private String TEST_POST_SHORT = "123456789";
    private String TEST_TITLE_VALID = "12345";
    private String TEST_POST_VALID = "1234567890";




    private WebDriver driver;
    private int implicitWait=20;
    private WebDriverWait explicitWait;

    @BeforeClass
    private void initDriver(){
        WebDriverManager.getInstance(ChromeDriver.class).setup();
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
        explicitWait = new WebDriverWait(driver,20);
    }

    @BeforeClass
    private void logIn(){
        driver.findElement(By.xpath(LOGIN_BUTTON_XPATH)).click();
        if (isLogInWindowOpened()) logInWithCredentials();
    }


    /**TC1*/
    @Test
    public void should_ShowProperMessage_when_TheTitleIsTooShort(){
        driver.findElement(By.id(NEW_TOPIC_ID)).click();
        driver.findElement(By.id(TITLE_FIELD_ID)).sendKeys(TEST_TITLE_SHORT);
        WebElement textArea = driver.findElement(By.cssSelector(TEXTAREA_FIELD_CSS));
        textArea.clear();
        textArea.sendKeys(TEST_POST_VALID);
        driver.findElement(By.xpath(CREATE_TOPIC_BUTTON_XPATH)).click();

        WebElement errMessage = driver.findElement(By.xpath(TITLE_TOO_SHORT_MESSAGE_XPATH));
        Assert.assertTrue(errMessage.isDisplayed(), TEST_FAIL_ERROR_MESSAGE);

    }

    /**TC2*/
    @Test
    public void should_ShowProperMessage_when_TheCommentIsTooShort(){
        driver.findElement(By.id(NEW_TOPIC_ID)).click();
        driver.findElement(By.id(TITLE_FIELD_ID)).sendKeys(TEST_TITLE_VALID);
        WebElement textArea = driver.findElement(By.cssSelector(TEXTAREA_FIELD_CSS));
        textArea.clear();
        textArea.sendKeys(TEST_POST_SHORT);
        driver.findElement(By.xpath(CREATE_TOPIC_BUTTON_XPATH)).click();

        WebElement errMessage = driver.findElement(By.xpath(POST_TOO_SHORT_MESSAGE_XPATH));
        Assert.assertTrue(errMessage.isDisplayed(),TEST_FAIL_ERROR_MESSAGE);
    }

    /**TC3*/
    @Test
    public void should_ShowProperMessages_when_BothCommentAndTitleAreTooShort(){
        driver.findElement(By.id(NEW_TOPIC_ID)).click();
        driver.findElement(By.id(TITLE_FIELD_ID)).sendKeys(TEST_TITLE_SHORT);
        WebElement textArea = driver.findElement(By.cssSelector(TEXTAREA_FIELD_CSS));
        textArea.clear();
        textArea.sendKeys(TEST_POST_SHORT);
        driver.findElement(By.xpath(CREATE_TOPIC_BUTTON_XPATH)).click();

        WebElement errMessage = driver.findElement(By.xpath(POST_TOO_SHORT_MESSAGE_XPATH));
        Assert.assertTrue(errMessage.isDisplayed(),TEST_FAIL_ERROR_MESSAGE);
        errMessage = driver.findElement(By.xpath(TITLE_TOO_SHORT_MESSAGE_XPATH));
        Assert.assertTrue(errMessage.isDisplayed(),TEST_FAIL_ERROR_MESSAGE);
    }


    @AfterMethod
    private void clearCreateTopicField()  {
        driver.findElement(By.xpath(CANCEL_TOPIC_BUTTON_XPATH)).click();
        WebElement confirmButton = driver.findElement(By.xpath(CONFIRM_CANCEL_BUTTON_XPATH));
        confirmButton.click();
        explicitWaitUntilCondition(ExpectedConditions.visibilityOfElementLocated(By.id(NEW_TOPIC_ID)));
    }

    @AfterClass
    public void closeDriver(){
        driver.close();
    }

    private boolean isLogInWindowOpened() {
        return (driver.getTitle().equals(SIGN_IN_WINDOW_TITLE));
    }

    private void logInWithCredentials(){
        driver.findElement(By.id(EMAIL_INPUT_ID)).sendKeys(USER);
        driver.findElement(By.id(PASSWORD_INPUT_ID)).sendKeys(PASS);
        driver.findElement(By.id(LOGIN_BUTTON_IN_SIGNIN_SCREEN_ID)).click();
    }

    private <T> T explicitWaitUntilCondition(ExpectedCondition<T> expectedCondition){
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        T result = explicitWait.withTimeout(Duration.ofSeconds(20)).until(expectedCondition);
        driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
        return result;
    }

}
