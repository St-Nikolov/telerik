## Scenarios for testing Telerik forum functionalities: 
* Creating topic
* Posting a reply
* Formatting text in reply and topic 
* Notifications 

### Priority levels: Low, Medium, High
### Methods used :
* **EP** - Equivalence partitioning
* **BVA** - Boundary value analysis 
* **DT** - Decision table
* **CT** - Classification tree
* **PW** - Pairwise 
* **ST** - State transition
* **EG** - Error guessing
* **CL** - Checked list



| N |Tests                                     |Priority       | Method      |
|:----:|:---                                      |:----:         |       :---: |
|1|Unregistered user should not be able to create any topic.   |High      | DT|
|2|Registered user should be able to create a topic in unrestricted categories and the ones explicitly opened for them. | High | DT | 
|3|Admins should be able to create a topic in every category. |Medium | DT |
|4|Unregistered user should not be able to post any reply.|High | DT |
|5|Registered user should be able to post a reply on unrestricted topics and in the ones explicitly opened for them. | High | DT |
|6|Admins should be able to post a reply on every topic. |Medium | DT |
|7|The reply field should resize adequately when you drag the forum window on a screen with a different resolution. |Low | EG |
|8|Topic title length should be between X and Y symbols. | Medium | BVA + EP |
|9|Topic and Reply length should be between X and Y symbols. | Medium | BVA + EP |
|10|All formatting: Quote, Hyperlink, List, Hidden text, Poll should work properly in a single post. | High | CL |
|11|Bold and Italic styles should work separately and together and in combination with any of the following: Quote, Hyperlink, List, Time/Date. | Medium | PW + CL |
|11|An user should be able to upload a picture from the local device and the web. |Medium| CL|
|12|An user should be able to insert icon from any category  |Low| EP + CL |
|13|An user should be able to insert date with and w/o time, with and w/o end date, with additional time zones. |Low| CT + CL |
|14|An user should be able to show/hide the preview of the post. |High |CL + ST |
|15|An user should be able to cancel writing of topic/reply, confirm dialogue should appear.|High|ST|
|16|Default notification level for a newly created topic should be "Watching" |Medium|CL|
|17|An user should be able to change the notification level of any topic he has access to. |Medium|CL|
|18|Notifications should be received for "Watching" topics. |Medium|DT|
|19|Notifications should be received when the user is tagged in a post or being replied to. |Medium|DT|
|20|Notifications should not be received, even on mentions, when the topic is "Muted". |Medium|DT|
|21|Number of the new posts should be shown when the topic is "Watching" or "Tracking". |Medium|DT|
|22|Number of the new post shouldn't be shown for "Normal" and "Muted" topics. |Low|DT|