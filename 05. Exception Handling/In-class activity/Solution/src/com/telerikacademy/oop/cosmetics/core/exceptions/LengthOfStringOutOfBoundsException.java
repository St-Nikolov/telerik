package com.telerikacademy.oop.cosmetics.core.exceptions;

public class InvalidDataException extends IllegalArgumentException{
    public InvalidDataException(String message) {
        super(message);
    }
}
