package com.telerikAcademy.onlineshop.contracts;

import com.telerikAcademy.onlineshop.models.Product;
import com.telerikAcademy.onlineshop.unums.Currency;

import java.time.LocalDate;
import java.util.List;

public interface Order {

    String getRecipient();

    Currency getCurrency();

    LocalDate getDeliveryOn();

    List<Product> getItems();

    void addItem(Product item);

    String getOrderItemsInfo();

    String getGeneralInfo();


}
