package com.telerikAcademy.onlineshop.models;

import com.telerikAcademy.onlineshop.unums.Currency;

public class Product {
    private static final int MIN_NAME_LENGTH = 2;
    private static final int MAX_NAME_LENGTH = 15;
    private String name;
    private double price;
    private Currency currency;

    public Product(String name, double price, Currency currency) {
        setName(name);
        setPrice(price);
        setCurrency(currency);
    }

    public Product(String name, double price) {
        this(name,price,Currency.BGN);
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        if(name==null) throw new IllegalArgumentException("Name if a product cannot be empty");
        if (name.length()< MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) throw new IllegalArgumentException(String.format("Product name length must be between %d and %d characters",MIN_NAME_LENGTH,MAX_NAME_LENGTH));
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public double getPriceInBGN(){
        return price*currency.getConversionRate();
    }

    private void setPrice(double price) {
        if (price <= 0.00) throw new IllegalArgumentException("Product price cannot be negative");
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    private void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDisplayInfo(){
        return String.format("%s - %.2f %s", name,price,currency);
    }
}
