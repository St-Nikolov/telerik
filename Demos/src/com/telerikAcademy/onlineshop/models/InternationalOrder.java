package com.telerikAcademy.onlineshop.models;

import com.telerikAcademy.onlineshop.unums.Currency;

import java.time.LocalDate;

public class InternationalOrder extends BaseOrder {
    public static final int MAX_PRODUCTS_LIMIT = 5;
    private String carrier;
    private int customsPercentage;

    public InternationalOrder(String recipient, Currency currency, LocalDate deliveryOn, String carrier, int customsPercentage) {
       super(recipient, currency, deliveryOn);
       setCarrier(carrier);
       setCustomsPercentage(customsPercentage);
    }

    private String getCarrier() {
        return carrier;
    }

    private void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public int getCustomsPercentage() {
        return customsPercentage;
    }

    public void setCustomsPercentage(int customsPercentage) {
        this.customsPercentage = customsPercentage;
    }

    @Override
    public String getGeneralInfo() {
        String info = String.format("%s | Carrier: %s | *NOTE* - customs of %d%% expected but to be calculated on arrival.",
                        super.getGeneralInfo(),
                getCarrier(),
                getCustomsPercentage());
        return info;
    }

    @Override
    public void addItem(Product item) {
        if (getItems().size() >= MAX_PRODUCTS_LIMIT) throw new IllegalArgumentException("International order are limited to 5 products");
        super.addItem(item);
    }

    @Override
    protected String getOrderType() {
        return "International";
    }
}
