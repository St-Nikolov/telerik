package com.telerikAcademy.onlineshop.models;

import com.telerikAcademy.onlineshop.contracts.Order;
import com.telerikAcademy.onlineshop.unums.Currency;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseOrder implements Order {
    private static final int MIN_RECIPIENT_NAME_LENGTH = 3;
    private static final int MAX_RECIPIENT_NAME_LENGTH = 35;
    private  static final int BGN_EUR_CONVERSION_RATE = 2;

    private String recipient;
    private Currency currency;
    private LocalDate deliveryOn;
    private List<Product> items;

    public BaseOrder(String recipient, Currency currency, LocalDate deliveryOn){
        setRecipient(recipient);
        setCurrency(currency);
        setDeliveryOn(deliveryOn);
        items = new ArrayList<>();
    }

    private void setRecipient(String recipient) {
        if (recipient == null) throw new IllegalArgumentException("Recipient cannon be empty");
        if (recipient.length() < MIN_RECIPIENT_NAME_LENGTH || recipient.length() > MAX_RECIPIENT_NAME_LENGTH)
            throw new IllegalArgumentException(String.format("Recipient name must be between %d and %d characters", MIN_RECIPIENT_NAME_LENGTH, MAX_RECIPIENT_NAME_LENGTH));

        this.recipient = recipient;
    }

    private void setCurrency(Currency currency) {
        this.currency = currency;
    }

    private void setDeliveryOn(LocalDate deliveryOn) {
        this.deliveryOn = deliveryOn;
    }

    public String getRecipient() {
        return recipient;
    }

    public Currency getCurrency() {
        return currency;
    }

    public LocalDate getDeliveryOn() {
        return deliveryOn;
    }

    public void addItem(Product  item){
        if(items.contains(item)) throw new IllegalArgumentException("This item is already in this order");
        items.add(item);
    }

    public List<Product> getItems(){
        return new ArrayList<>(items);
    }

    public double calculateTotalPrice() {
        double total = 0;
        for (Product item:items) {
            total+=item.getPriceInBGN();
        }
        total=total/currency.getConversionRate();
        return total;
    }

    public String getOrderItemsInfo(){
        if (items.size()==0) System.out.println("No items");
        StringBuilder builder = new StringBuilder();
        for (Product item:items) {
            builder.append(String.format("%s%n",item.getDisplayInfo()));
        }
        return(builder.toString());
    }

    public String getGeneralInfo(){
        String info = String.format("%s Order to : %s | Delivery on : %s | Total : %.2f %s",
                getOrderType(),
                recipient,
                deliveryOn.toString(),
                calculateTotalPrice(),
                currency);
        return info;
    }

    protected abstract String getOrderType();

}
