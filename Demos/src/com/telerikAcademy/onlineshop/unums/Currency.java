package com.telerikAcademy.onlineshop.unums;

public enum Currency {
    BGN(1),EUR(1.95),USD(1.75);

    private double conversionRate;

    private Currency(double conversionRate){
        this.conversionRate = conversionRate;
    }

    public double getConversionRate(){
        return conversionRate;
    }
}
