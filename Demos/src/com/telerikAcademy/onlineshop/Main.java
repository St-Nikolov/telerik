package com.telerikAcademy.onlineshop;

import com.telerikAcademy.onlineshop.contracts.Order;
import com.telerikAcademy.onlineshop.models.DomesticOrder;
import com.telerikAcademy.onlineshop.models.InternationalOrder;
import com.telerikAcademy.onlineshop.models.BaseOrder;
import com.telerikAcademy.onlineshop.models.Product;
import com.telerikAcademy.onlineshop.unums.Currency;

import java.time.LocalDate;
import java.time.Month;

public class Main {

    public static void main(String[] args) {

        Order order1 = new DomesticOrder("John", Currency.BGN, LocalDate.now());
        order1.addItem(new Product("Keyboard",50));
        order1.addItem(new Product("Monitor",100));

        Order order2 = new DomesticOrder("Frank",Currency.BGN,LocalDate.now());
        order2.addItem(new Product("T-Shirt",10,Currency.USD));
        order2.addItem(new Product("Jeans",20,Currency.USD));

        Order order3 = new InternationalOrder(
                "Pencho",
                Currency.EUR,
                LocalDate.of(2021, Month.APRIL,25),
                "DHL",
                20);

        order3.addItem(new Product("Shirt",10));
        order3.addItem(new Product("Backpack",60));


        Order[] orders = new Order[]{order1,order2,order3};

        for (Order order:orders) {
            System.out.println(order.getGeneralInfo());
            System.out.println();
            System.out.println(order.getOrderItemsInfo());
            System.out.printf("%n ------------------------------------- %n");
        }

    }
}
