package com.telerikAcademy.intefacesdemo;

import com.telerikAcademy.intefacesdemo.enums.Directions;
import com.telerikAcademy.intefacesdemo.interfaces.Movable;

import static com.telerikAcademy.intefacesdemo.enums.Directions.UP;

public class Point implements Movable {
    public static final String UNSUPPORTED_DIRECTION_ERR = "Unsupported direction!";
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void move(Directions direction,double distance){
        switch (direction) {
            case UP:
                y-=distance;
                break;
            case DOWN:
                y+=distance;
                break;
            case LEFT:
                x-=distance;
                break;
            case RIGHT:
                x+=distance;
                break;
            default: throw new IllegalArgumentException(UNSUPPORTED_DIRECTION_ERR);
        }
    }

    @Override
    public String toString() {
        return String.format("X: %.2f, Y: %.2f",x,y);
    }
}
