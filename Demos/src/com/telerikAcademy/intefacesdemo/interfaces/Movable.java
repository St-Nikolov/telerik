package com.telerikAcademy.intefacesdemo.interfaces;

import com.telerikAcademy.intefacesdemo.enums.Directions;

public interface Movable {
    void move(Directions direction,double distance);
}
