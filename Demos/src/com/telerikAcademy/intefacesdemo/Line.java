package com.telerikAcademy.intefacesdemo;

import com.telerikAcademy.intefacesdemo.enums.Color;
import com.telerikAcademy.intefacesdemo.enums.Directions;
import com.telerikAcademy.intefacesdemo.interfaces.Movable;

public class Line implements Movable {
    private Color color;
    private Point firstEnd;
    private Point secondEnd;

    public Line(Color color, Point firstEnd, Point secondEnd) {
        this.color = color;
        this.firstEnd = firstEnd;
        this.secondEnd = secondEnd;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public void move(Directions direction, double distance) {
        firstEnd.move(direction, distance);
        secondEnd.move(direction, distance);
    }

    @Override
    public String toString() {
        return String.format("A %s line with first end at %s and second end at %s",color,firstEnd,secondEnd);
    }
}
