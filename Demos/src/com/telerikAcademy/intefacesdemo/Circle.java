package com.telerikAcademy.intefacesdemo;

import com.telerikAcademy.intefacesdemo.enums.Color;
import com.telerikAcademy.intefacesdemo.enums.Directions;
import com.telerikAcademy.intefacesdemo.interfaces.Movable;

public class Circle extends Shape implements Movable {
    public static final String NEGATIVE_RADIUS_ERR = "Radius of the circle should not be negative number";
    private Point center;
    private double radius;

    public Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    public void setRadius(double radius) {
        if (radius<0) throw new IllegalArgumentException(NEGATIVE_RADIUS_ERR);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    @Override
    public void move(Directions direction, double distance) {
        center.move(direction, distance);
    }
}
