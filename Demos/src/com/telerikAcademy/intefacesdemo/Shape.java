package com.telerikAcademy.intefacesdemo;

import com.telerikAcademy.intefacesdemo.enums.Color;

public abstract class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    protected abstract double getArea();

}
