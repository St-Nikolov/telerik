package com.telerikAcademy.intefacesdemo;

import com.telerikAcademy.intefacesdemo.enums.Color;
import com.telerikAcademy.intefacesdemo.enums.Directions;
import com.telerikAcademy.intefacesdemo.interfaces.Movable;

public class Rectangle extends Shape implements Movable {
    public static final String NEGATIVE_SIDE_LENGTH_ERR = "Sides of the rectangle should not be negative length";
    private Point center;
    private double length;
    private double height;

    public Rectangle(Color color, Point center, double length, double height) {
        super(color);
        checkLineLength(length);
        checkLineLength(height);
        this.center = center;
        this.length = length;
        this.height = height;
    }

    @Override
    public double getArea() {
        return length*height;
    }

    @Override
    public void move(Directions direction, double distance) {
        center.move(direction, distance);
    }

    private void checkLineLength(double lineLength){
        if (lineLength<0) throw new IllegalArgumentException(NEGATIVE_SIDE_LENGTH_ERR);
    }
}
