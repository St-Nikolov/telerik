package com.telerikAcademy;

import java.time.LocalDate;

public class Issue extends BoardItem{

    private static final BoardItemStatus INITIAL_STATUS = BoardItemStatus.OPEN;
    private static final BoardItemStatus FINAL_STATUS = BoardItemStatus.VERIFIED;
    public static final String EMPTY_DESCRIPTION = "No description";

    private final String description;

    public Issue(String title, String description, LocalDate dueDate){
        super(title, dueDate);
        if (description==null || description.isBlank()) this.description= EMPTY_DESCRIPTION;
        else this.description=description;
    }

    public void revertStatus() {
        if (status == FINAL_STATUS)  {
            status = INITIAL_STATUS;
            addLogEvent("Issue status",FINAL_STATUS.toString(), INITIAL_STATUS.toString());
        } else {
            log.add(new EventLog(CANNOT_REVERSE_STATUS + getStatus()));
        }
    }

    public void advanceStatus() {
        if (status == INITIAL_STATUS) {
            status = FINAL_STATUS;
            addLogEvent("Issue status",INITIAL_STATUS.toString(), FINAL_STATUS.toString());
        } else {
            log.add(new EventLog(CANNOT_ADVANCE_STATUS + getStatus()));
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Issue: %s, Description: %s",baseInfo,description);
    }


    protected void setInitialStatus(){
        status = INITIAL_STATUS;
    }


}
