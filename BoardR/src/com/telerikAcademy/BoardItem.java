package com.telerikAcademy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardItem {

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private static final String EMPTY_BOARD_ITEM_TITLE_ERR = "Title of a board item should not be empty!";
    private static final String LENGTH_BOARD_ITEM_TITLE_ERR = "Please provide a title with length between %d and %d chars";
    private static final String DUE_DATE_IN_THE_PAST_ERR = "Due date can not be in the past";
    private static final String STATUS_CHANGED_INFO = "%s changed from %s to %s";
    protected static final String CANNOT_REVERSE_STATUS = "Cannot reverse, status already at ";
    protected static final String CANNOT_ADVANCE_STATUS = "Cannot advance, status already at ";
    private static final String ITEM_CREATED = "Item created: '%s', [%s | %s]";

    private String title;
    private LocalDate dueDate;
    protected BoardItemStatus status;
    protected List<EventLog> log;


    protected BoardItem(String title,LocalDate dueDate){
        setTitle(title);
        setDueDate(dueDate);
        setInitialStatus();
        log = new ArrayList<>();
        log.add(new EventLog(String.format(ITEM_CREATED, title, getStatus(),getDueDate())));
    }

    void setTitle(String title) {
        String oldTitle = this.title;
        if (title == null || title.isBlank())
            throw new IllegalArgumentException(EMPTY_BOARD_ITEM_TITLE_ERR);
        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH)
            throw new IllegalArgumentException(String.format(LENGTH_BOARD_ITEM_TITLE_ERR, MIN_TITLE_LENGTH, MAX_TITLE_LENGTH));
        this.title = title;
        if (oldTitle!=null)
            addLogEvent("Title",oldTitle,title);
    }

    void setDueDate(LocalDate dueDate) {
        LocalDate oldDueDate = this.dueDate;
        if (LocalDate.now().isAfter(dueDate)) throw new IllegalArgumentException(DUE_DATE_IN_THE_PAST_ERR);
        this.dueDate = dueDate;
        if(oldDueDate!=null) addLogEvent("Due date", oldDueDate.toString(), dueDate.toString());
    }

    public String getTitle() {
        return title;
    }

    public String getStatus() {
        return status.toString();
    }


    public LocalDate getDueDate() {
        return dueDate;
    }

    public String viewInfo() {
        return String.format("\'%s\', [%s | %s]", title, status, dueDate);
    }


    abstract public void revertStatus();

    abstract public void advanceStatus();

    abstract protected void setInitialStatus();


    protected void addLogEvent(String property,String oldValue,String newValue) {
        log.add(new EventLog(String.format(STATUS_CHANGED_INFO, property, oldValue, newValue)));
    }

    public String getHistory(){
        StringBuilder history = new StringBuilder();
        for (EventLog eventLog:log) {
            history.append(eventLog.viewInfo() + "\n");
        }
        return  history.toString().trim();
    }
}
