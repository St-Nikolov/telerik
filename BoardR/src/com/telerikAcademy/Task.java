package com.telerikAcademy;

import java.time.LocalDate;

public class Task extends BoardItem{

    private static final BoardItemStatus INITIAL_STATUS = BoardItemStatus.TO_DO;
//    private static final BoardItemStatus FINAL_STATUS = BoardItemStatus.VERIFIED;

    private static final String LENGTH_ASSIGNEE_NAME_ERR = "Please provide an assignee name with length between %d and %d chars";
    private static final String ASSIGNEE_NAME_EMPTY_ERR = "Assignee name cannot be empty";
    private static final int MIN_ASSIGNEE_NAME_LENGTH = 5;
    private static final int MAX_ASSIGNEE_NAME_LENGTH = 30;


    private String assignee;

    public Task(String title,String assignee, LocalDate dueDate) {
        super(title, dueDate);
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }



    public void setAssignee(String assignee) {
        String oldAssignee = this.assignee;
        if (assignee == null || assignee.isBlank())
            throw new IllegalArgumentException(ASSIGNEE_NAME_EMPTY_ERR);
        if (assignee.length() < MIN_ASSIGNEE_NAME_LENGTH || assignee.length() > MAX_ASSIGNEE_NAME_LENGTH)
            throw new IllegalArgumentException(String.format(LENGTH_ASSIGNEE_NAME_ERR, MIN_ASSIGNEE_NAME_LENGTH, MAX_ASSIGNEE_NAME_LENGTH));
        this.assignee = assignee;
        if (oldAssignee!=null)
            addLogEvent("Assignee",oldAssignee,assignee);

    }

    public void revertStatus() {
        if (status != BoardItemStatus.values()[0])  {
            String oldStatus = getStatus();
            status = BoardItemStatus.values()[status.ordinal() -1];
            addLogEvent("Task status",oldStatus, getStatus());
        } else {
            log.add(new EventLog(CANNOT_REVERSE_STATUS + getStatus()));
        }
    }

    public void advanceStatus() {
        if (status != BoardItemStatus.values()[BoardItemStatus.values().length-1]) {
            String oldStatus = getStatus();
            status = BoardItemStatus.values()[status.ordinal() +1 ];
            addLogEvent("Task status",oldStatus, getStatus());
        } else {
            log.add(new EventLog(CANNOT_ADVANCE_STATUS + getStatus()));
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Task: %s, Assignee: %s",baseInfo,getAssignee());
    }

    protected void setInitialStatus(){
        status = INITIAL_STATUS;
    }


}
