package com.telerikAcademy;

public interface Logger {
    void log(String value);
}
