package com.telerikAcademy;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private Board(){          }

    private static List<BoardItem> items = new ArrayList<>();

    public static void addItem(BoardItem boardItem) {
        if (items.contains(boardItem)) throw new IllegalArgumentException(String.format("Item \"%s\" already in the list", boardItem.viewInfo()));
        items.add(boardItem);
    }

    public static int totalItems(){
        return items.size();
    }

    public static void displayHistory(Logger logger){
        for (BoardItem item:items) {
            logger.log(item.getHistory());
        }
    }

}
