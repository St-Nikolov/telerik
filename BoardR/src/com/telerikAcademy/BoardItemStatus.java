package com.telerikAcademy;

public enum BoardItemStatus {
    OPEN("Open"),
    TO_DO("To Do"),
    IN_PROGRESS("In Progress"),
    DONE("Done"),
    VERIFIED("Verified");

    private final String label;

    private BoardItemStatus(String label){
        this.label=label;
    }

    @Override
    public String toString() {
        return label;
    }

}
