package com.telerikAcademy;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLog {
    public static final String LOG_DESCRIPTION_CAN_NOT_BE_NULL = "Log description can not be null";
    public static final String DATE_TIME_FORMAT_PATTERN = "[dd-MMMM-yyyy hh:mm:ss]";
    private final String description;
    private final LocalDateTime timestamp;

    public EventLog(String description) {
        if (description==null) throw new IllegalArgumentException(LOG_DESCRIPTION_CAN_NOT_BE_NULL);
        this.description=description;
        timestamp = LocalDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String viewInfo(){
        return "[" + getTimestamp().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN)) + "]" + " " + getDescription();
    }
}
