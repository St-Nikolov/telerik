package com.telerikacademy.simpletests;

import java.util.Arrays;

public class MyListImpl<T> implements MyList {

    private static final int DEFAULT_CAPACITY=5;

    private T[] elements;
    private int size;

    public MyListImpl(int initialCapacity) {
        elements = createArray(initialCapacity);
    }

    public MyListImpl() {
        this(DEFAULT_CAPACITY);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return elements.length;
    }

    @Override
    public void add(Object element) {
        if (size() == capacity()) resize();

        elements[size] = (T)element;
        size ++;
    }

    @Override
    public Object get(int index) {
        validateIndex(index);

        return elements[index];
    }

    @Override
    public int indexOf(Object element) {
        for (int i = 0; i < size; i++){
            if (elements[i].equals(element))
                return i;
        }
        return -1;
    }

    @Override
    public boolean remove(Object element) {
        int i = indexOf(element);
        if (i == -1)
            return false;

        removeAt(i);
        return true;
    }

    @Override
    public void removeAt(int index) {
        validateIndex(index);

        size--;

        System.arraycopy(
                elements,
                index+1,
                elements,
                index,
                size-index);
    }

    private T[] createArray(int capacity) {
        return (T[]) new Object[capacity()];
    }

    private void validateIndex(int index){
        if (index>=size) throw new IllegalArgumentException();
    }

    private void resize(){
        elements = Arrays.copyOf(elements,size() * 2);
    }

}
