urlField = "urlField.png"
loginButton = "1620830590923.png"
emailField = "1620830621115.png"
passField = "1620830641361.png"
newTopicButton = "1620830704136.png"
title = "title.png"
post = "post.png"
createTopicButton = "createTopicButton.png"
titleErr = "titleErr.png"
postErr = "postErr.png"
clearDraftConfirmButton = "1620830880175.png"
cancelButton = "cancelTopic.png"
type(Key.CMD)
wait(1)
type("chrome -incognito" + Key.ENTER)
wait(urlField,5)
paste(urlField,"https://stage-forum.telerikacademy.com/")
type(Key.ENTER)
wait(loginButton,10)
click(loginButton)
wait(emailField,5)
paste(emailField,"stanislav.nikolov.a28@learn.telerikacademy.com")
paste(passField,"Mechka")
type(Key.ENTER)
wait(newTopicButton,5)
click(newTopicButton)
paste(title,"1234")
paste(post,"123456789")
click(createTopicButton)
try:
    assert exists(titleErr,2)
except AssertionError:
    print "Title length verification failed"
try:   
    assert exists(postErr,2)
except AssertionError:
    print "Post length verification failed"
click(cancelButton)
wait(clearDraftConfirmButton,2)
click(clearDraftConfirmButton)
type(Key.F4,Key.ALT)